package ru.test.services;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.test.exception.ResourceException;
import ru.test.jpa.User;
import ru.test.repository.UserDAO;

import javax.validation.ConstraintViolation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {

    private final static Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserDAO userRepository;

    @Autowired
    private ShaPasswordEncoder passwordEncoder;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        if (StringUtils.isBlank(login)) {
            throw new UsernameNotFoundException("User name is empty");
        }
        final User user = userRepository.findByLogin(login);

        if (user == null) {
            throw new UsernameNotFoundException("No user found");
        }
        return new org.springframework.security.core.userdetails.User(
                user.getLogin(),
                user.getPassword(),
                getGrantedAuthorities(user.getRoleNames()));
    }

    public static List<GrantedAuthority> getGrantedAuthorities(String[] roles) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (String role : roles) {
            authorities.add(new SimpleGrantedAuthority(role));
        }
        return authorities;
    }

    @Override
    @Transactional(rollbackFor = ResourceException.class)
    public User create(User user) throws ResourceException {
        try {
            final User saved = userRepository.save(user);
            return saved;
        } catch (DataIntegrityViolationException ex) {
            final ConstraintViolationException cause = (ConstraintViolationException) ex.getCause();
            final String constraintName = cause.getConstraintName();
            if (StringUtils.equalsIgnoreCase(constraintName, User.UK_EMAIL)) {
                throw new ResourceException("User.Errors.UniqueEmail");
            } else {
                LOGGER.warn("{}: {}", constraintName, cause.getMessage());
                throw new ResourceException("UnknownError");
            }
        } catch (javax.validation.ConstraintViolationException ex) {
            final ResourceException ue = new ResourceException();
            for (ConstraintViolation cv : ex.getConstraintViolations()) {
                final String propertyPath = cv.getPropertyPath().toString();
                if (propertyPath.equals("firstName")) {
                    ue.getMessages().add("User.Errors.ShortFirstName");
                } else if (propertyPath.equals("lastName")) {
                    ue.getMessages().add("User.Errors.ShortLastName");
                } else if (propertyPath.equals("login")) {
                    ue.getMessages().add("User.Errors.ShortLogin");
                } else if (propertyPath.equals("email")) {
                    ue.getMessages().add("User.Errors.WrongEmail");
                }

            }
            LOGGER.debug("Throwing violations {}", StringUtils.join(ue.getMessages(), ","));
            throw ue;
        }

    }

    @Override
    @Transactional(readOnly = true)
    public User findOne(final String login) {
        return userRepository.findByLogin(login);
    }

    @Override
    @Transactional(readOnly = true)
    public User findOne(final Long userId) {
        return userRepository.findOne(userId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<User> findAll() {
        Iterable<User> users = userRepository.findAll();
        return Lists.newArrayList(users);
    }

    @Override
    @Transactional
    public User editUser(User user) {
        final User persistentUser = userRepository.findByLogin(user.getLogin());
        persistentUser.setEmail(user.getEmail());
        persistentUser.setFirstName(user.getFirstName());
        persistentUser.setLastName(user.getLastName());
        persistentUser.setUserRole(user.getUserRole());

        return userRepository.save(persistentUser);
    }

    @Override
    @Transactional
    public void delete(User entity) {
        userRepository.delete(entity.getId());
    }

    @Override
    @Transactional(readOnly = true)
    public User findOne(User entity) {
        return userRepository.findOne(entity.getId());
    }

    @Override
    @Transactional(readOnly = true)
    public List<User> search(String query) {
        if (org.apache.commons.lang3.StringUtils.isBlank(query)) {
            return Collections.emptyList();
        }
        return userRepository.search(query);
    }


}
