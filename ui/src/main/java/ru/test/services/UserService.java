package ru.test.services;

import org.springframework.security.core.userdetails.UserDetailsService;
import ru.test.exception.ResourceException;
import ru.test.jpa.User;

import java.util.List;

public interface UserService extends UserDetailsService, ResourceService<User> {
    public User findOne(String email);
    public User create(User userCreateForm) throws ResourceException;
    public User editUser(User user);

    public List<User> search(String query);
}
