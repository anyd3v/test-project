package ru.test.services;

import ru.test.exception.ResourceException;

import java.util.List;

public interface ResourceService<T> {
    T create(T entity) throws ResourceException;
    T findOne(T entity);
    T findOne(Long id);
    List<T> findAll();
    void delete(T entity);
}
