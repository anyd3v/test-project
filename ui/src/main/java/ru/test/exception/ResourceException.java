package ru.test.exception;

import java.util.ArrayList;
import java.util.List;

public class ResourceException extends Exception {

    private final List<String> messages = new ArrayList<>();
    
    public ResourceException() {
    }

    public ResourceException(String message) {
        super(message);
        messages.add(message);
    }

    public ResourceException(String message, Throwable cause) {
        super(message, cause);
        messages.add(message);
    }

    public ResourceException(Throwable cause) {
        super(cause);
    }

    public ResourceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public List<String> getMessages() {
        return messages;
    }
}
