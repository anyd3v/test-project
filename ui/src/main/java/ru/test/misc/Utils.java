package ru.test.misc;

import org.springframework.context.MessageSource;
import org.springframework.security.crypto.bcrypt.BCrypt;
import ru.test.jpa.SystemRole;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class Utils {

    public static String bcrypt(final String secret) {
        return BCrypt.hashpw(secret, BCrypt.gensalt());
    }
    
    public static Map<String, String> localizedRoles(List<SystemRole> roles, MessageSource messageSource, Locale locale) {
        final Map<String, String> map = new HashMap<>();
        for(SystemRole r : roles) {
            map.put(r.getName(), messageSource.getMessage(r.getName(), null, locale));
        }
        return  map;
    }
    
}
