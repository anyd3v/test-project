package ru.test.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.test.exception.ResourceException;
import ru.test.jpa.User;
import ru.test.misc.Utils;
import ru.test.services.UserService;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Controller
@RequestMapping("/user")
public class UserController {

    private final static Logger LOGGER = LoggerFactory.getLogger(UserController.class);
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private MessageSource messageSource;
    
    
    @ModelAttribute("currentUser")
    public User getLoggedUser() {
        try {
            final org.springframework.security.core.userdetails.User principal = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return userService.findOne(principal.getUsername());
        } catch(Exception ex) {
            return null;
        }
    }

    @RequestMapping("/create")
    public String createPage(Model model, Principal principal, Locale locale) {
        final User loggedUser = getLoggedUser();
        
        User userCreateForm = new User();
        model.addAttribute("userCreateForm", userCreateForm);
        model.addAttribute("roles", Utils.localizedRoles(loggedUser.getUserRole(), messageSource, locale));
        return "user/create";
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String create(@ModelAttribute User userForm,
            BindingResult bindingResult,
            Model model,
            Locale locale,
            RedirectAttributes redirectAttributes) {
        if(bindingResult.hasErrors()) {
            return "create";
        }
        try {
            userService.create(userForm);
            redirectAttributes.addFlashAttribute("success", messageSource.getMessage("UI.Messages.User.CreatedSuccess", null, locale));
        } catch (ResourceException ue) {
            final User loggedUser = getLoggedUser();
            final List<String> failures = new ArrayList<>();
            for(String m : ue.getMessages()) failures.add( messageSource.getMessage(m, null, locale) );
            model.addAttribute("failures", failures);
            model.addAttribute("userForm", userForm);
            model.addAttribute("roles", Utils.localizedRoles(loggedUser.getUserRole(), messageSource, locale));
            return "user/create";
        }
        return "redirect:/";
    }

    @RequestMapping("/edit")
    public String editPage(Model model, Locale locale) {
        final User loggedUser = getLoggedUser();
        model.addAttribute("user", loggedUser);
        model.addAttribute("roles", Utils.localizedRoles(loggedUser.getUserRole(), messageSource, locale));
        return "user/edit";
    }

    @RequestMapping("/view")
    public String viewPage(Model model) {
        final User user = getLoggedUser();
        model.addAttribute("userCreateForm", user);
       
        return "user/view";
    }
    
    @RequestMapping("/{userId}/edit")
    public String editPageById(@PathVariable Long userId, Model model, Locale locale) {
        final User currentUser = getLoggedUser();
        final User user = userService.findOne(userId);
        if(user == null) {
            return "404";
        }

        final User userCreateForm = user;
        model.addAttribute("userCreateForm", userCreateForm);
        model.addAttribute("roles", Utils.localizedRoles(currentUser.getUserRole(), messageSource, locale));
        model.addAttribute("user", user);

        return "user/edit";
    }
    
    @RequestMapping("/{userId}/view")
    public String viewPageById(@PathVariable Long userId, Model model, Locale locale) {
        final User currentUser = getLoggedUser();
        final User user = userService.findOne(userId);
        if(user == null) {
            return "404";
        }

        final User userCreateForm = user;
        model.addAttribute("userCreateForm", userCreateForm);
        
        return "user/view";
    }
    
    @RequestMapping(value = "/{userId}/edit", method = RequestMethod.POST)
    public String edit(@PathVariable Long userId, @ModelAttribute User userCreateForm, BindingResult bindingResult, Model model) {
        final User currentUser = getLoggedUser();
        User user = userService.findOne(userId);
        user = userService.editUser(userCreateForm);

        return String.format("redirect:/user/%d/view", user.getId());
    }
    
    @RequestMapping(value = "/search")
    public String search(@RequestParam (required = false) String query, Model model) {
        final List<User> users = userService.search(query);
        model.addAttribute("query", query);
        model.addAttribute("users", users);
        return "user/userList";
    }
}
