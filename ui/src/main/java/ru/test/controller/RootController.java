package ru.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.test.jpa.User;
import ru.test.services.UserService;

import java.security.Principal;
import java.util.Locale;


@Controller
public class RootController {
	
    @Autowired
    private UserService userService;

    @Autowired
    private ShaPasswordEncoder passwordEncoder;
    
    @ModelAttribute("currentUser")
    public User getLoggedUser() {
        try {
            final org.springframework.security.core.userdetails.User principal = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return userService.findOne(principal.getUsername());
        } catch(Exception ex) {
            return null;
        }
    }
    
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model, Principal principal) {

        System.out.println(passwordEncoder.encodePassword("123456", null));

        if(principal==null) {
            return "login";
        }
		return "user/dashboard";
	}

    @RequestMapping("/login")
    public String login() {
        return "login";
    }
    
    @RequestMapping("/403")
    public String _403() {
        return "403";
    }
    
    @RequestMapping("/404")
    public String _404() {
        return "404";
    }

}
