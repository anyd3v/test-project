<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!doctype html>
<html>
    <head>
        <title>Login Page</title>
        <%@include file="/WEB-INF/jspf/resources.jspf" %>
    </head>
    <body>
        <%@include file="/WEB-INF/jspf/resources.jspf" %>
        <div class="col-md-6 col-md-offset-3" style="margin-top: 30px">
        <c:if test="${not empty SPRING_SECURITY_LAST_EXCEPTION}">
            <p class="bg-danger">
                Invalid username or password.
            </p>
        </c:if>
        <form name="login_form" action="<c:url value='j_spring_security_check' />" method='POST' class="form-horizontal" role="form">

            <div class="form-group">
                <label for="email" class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                    <input type="email" class="form-control" name="email" placeholder="Email">
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="col-sm-2 control-label">Password</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control" name="password" placeholder="Password">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="rememberme"> Remember me
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">Sign in</button>
                </div>
            </div>
        </form>
        </div>
        <%@include file="/WEB-INF/jspf/footer.jspf" %>
        <script>
            $(document).ready(function(){
                $("form input[name=email]:first").focus();
            });
        </script>
    </body>
</html>