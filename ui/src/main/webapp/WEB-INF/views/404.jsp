<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
    <head>
        <title><c:out value="${initParam.applicationName} - 404"/></title>

        <%@include file="/WEB-INF/jspf/resources.jspf" %>
    </head>
    <body>
        <%@include file="/WEB-INF/jspf/header.jspf" %>
        <div class="b-header-submenu row">
            <div class="col-md-12">
                <h2 class="b-header-submenu_title col-md-offset-2">404 <spring:message code="Errors.NotFound"/></h2>
            </div>
        </div>
        <%@include file="/WEB-INF/jspf/footer.jspf" %>
    </body>
</html>