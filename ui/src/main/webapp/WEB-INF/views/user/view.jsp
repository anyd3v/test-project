<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <spring:message code="UserDetails" var="msgUserDetails"/>
        <title><c:out value="${initParam.applicationName} - ${msgUserDetails}"/></title>

        <%@include file="/WEB-INF/jspf/resources.jspf" %>
    </head>
    <body>
        <%@include file="/WEB-INF/jspf/header.jspf" %>
        <c:choose>
            <c:when test="${not empty insufficientRights}">
                <p><c:out value="${insufficientRights}"/></p>
            </c:when>

            <c:otherwise>
                
                <div class="col-md-5 col-md-offset-1">
                    <c:choose>
                        <c:when test="${currentUser.id == userCreateForm.id}">
                            <a href="/user/${userCreateForm.id}/edit"><spring:message code="Edit"/></a>
                        </c:when>
                        <c:otherwise>
                            <sec:authorize access="hasRole('ROLE_ADMIN')">
                                <a href="/user/${userCreateForm.id}/edit"><spring:message code="Edit"/></a>
                            </sec:authorize>
                        </c:otherwise>
                    </c:choose>
                    <table class="b-table table table-striped">
                        <tbody>
                            <tr>
                                <td><spring:message code="UI.Labels.User.FirstName"/></td>
                                <td>${userCreateForm.firstName}</td>
                            </tr>
                            <tr>
                                <td><spring:message code="UI.Labels.User.LastName"/></td>
                                <td>${userCreateForm.lastName}</td>
                            </tr>
                            <tr>
                                <td><spring:message code="UI.Labels.User.Email"/></td>
                                <td>${userCreateForm.email}</td>
                            </tr>
                            <tr>
                                <td><spring:message code="UI.Labels.User.Role"/></td>
                                <td>
                                    <c:forEach items="${userCreateForm.userRole}" var="role">
                                    <spring:message code="${role.name}"/>
                                    </c:forEach>
                                </td>
                            </tr>
                            <tr>
                                <td><spring:message code="UI.Labels.User.Registered"/></td>
                                <td><fmt:formatDate value="${userCreateForm.createAt.time}" pattern="dd.MM.yyyy"/></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </c:otherwise>
        </c:choose>  
        <%@include file="/WEB-INF/jspf/footer.jspf" %>
    </body>
</html>
