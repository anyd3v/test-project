<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <spring:message code="Users" var="msgUsers"/>
        <title><c:out value="${initParam.applicationName} - ${msgUsers}"/></title>

        <%@include file="/WEB-INF/jspf/resources.jspf" %>
    </head>
    <body>
        <%@include file="/WEB-INF/jspf/header.jspf" %>

        <div class="col-md-10 col-md-offset-1">
            <!--<h4>Search result</h4>-->
            <table class="b-table b-table--search table table-striped">
                <thead>
                    <tr>
                        <th><spring:message code="UI.Labels.User.Created"/></th>
                        <th><spring:message code="UI.Labels.User.FirstName"/></th>
                        <th><spring:message code="UI.Labels.User.LastName"/></th>
                        <th><spring:message code="UI.Labels.User.Email"/></th>
                        <th><spring:message code="UI.Labels.User.Role"/></th>
                        <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${users}" var="user" varStatus="status">
                        <tr>      
                            <td><fmt:formatDate pattern="dd.MM.yyyy" type="date" value="${user.createAt.time}"/></td>
                            <td>${user.firstName}</td>
                            <td>${user.lastName}</td>
                            <td>${user.email}</td> 
                            <td>
                                <c:forEach items="${user.userRole}" var="role">
                                    <spring:message code="${role.name}"/>
                                </c:forEach>
                            </td>
                            <td>
                            <sec:authorize access="hasRole('ROLE_ADMIN')">
                                <a href="${pageContext.request.contextPath}/user/${user.id}/edit"><span class="glyphicon glyphicon-edit"></span></a>
                            </sec:authorize>
                                <a href="${pageContext.request.contextPath}/user/${user.id}/view"><span class="glyphicon glyphicon-info-sign"></span></a>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>


        <%@include file="/WEB-INF/jspf/footer.jspf" %>
    </body>
</html>
