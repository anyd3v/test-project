<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <spring:message code="Home" var="msgDashboard"/>
        <title>${initParam.applicationName} - ${msgDashboard}</title>

        <%@include file="/WEB-INF/jspf/resources.jspf" %>
        <!--[if lt IE 9]>
            <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <script>window.html5 || document.write('<script src="${pageContext.request.contextPath}/resources/js/vendor/html5shiv.js"><\/script>')</script>
        <![endif]-->
    </head>
    <body>
        <%@include file="/WEB-INF/jspf/header.jspf" %>
        <div class="jumbotron">
            <h1><spring:message code="Hello"/>, ${currentUser.fullName}</h1>
            <p><spring:message code="YouCan"/>: 
            <ul>
                <sec:authorize access="hasRole('ROLE_ADMIN')">
                    <li><spring:message code="CreateUsers"/></li>
                    <li><spring:message code="EditUsers"/></li>
                    <li><spring:message code="SearchUsers"/></li>
                </sec:authorize>

                <sec:authorize access="hasRole('ROLE_MANAGER')">
                    <li><spring:message code="SearchUsers"/></li>
                    <li><spring:message code="ViewUsers"/></li>
                    <li><spring:message code="EditYourself"/></li>

                </sec:authorize>

                <sec:authorize access="hasRole('ROLE_USER')">
                    <li><spring:message code="EditYourself"/></li>
                    <li><spring:message code="ViewYourself"/></li>
                </sec:authorize>
            </ul>
        </p>
    </div>

    <%@include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>
