<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <spring:message code="User.CreateUser" var="msgCreateUser"/>
        <title><c:out value="${initParam.applicationName} - ${msgCreateUser}"/></title>
        <%@include file="/WEB-INF/jspf/resources.jspf" %>
    </head>
    <body>
        <%@include file="/WEB-INF/jspf/header.jspf" %>

        <div class="container i-line">
            <div class="row">

                <div class="col-md-5 col-md-offset-1">
                    <c:if test="${not empty failures}">
                        <div id="flash-message" class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <c:forEach items="${failures}" var="f">
                                <p>${f}</p>
                            </c:forEach>
                        </div>
                    </c:if>
                    <form:form action="${pageContext.request.contextPath}/user/create" method="post" modelAttribute="userCreateForm" autocomplete="off">
                        <table class="b-table table table-striped">
                            <tbody>

                                <tr>
                                    <td><spring:message code="UI.Labels.User.FirstName"/></td>
                                    <td>
                                        <form:input type="text" path="firstName" cssClass="form-control"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td><spring:message code="UI.Labels.User.LastName"/></td>
                                    <td>
                                        <form:input type="text" path="lastName" cssClass="form-control"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td><spring:message code="UI.Labels.User.Role"/></td>
                                    <td>
                                        <form:select path="userRole" cssClass="form-control">
                                            <form:options items="${roles}"/>
                                        </form:select>
                                    </td>
                                </tr>


                                <tr>
                                    <td><spring:message code="UI.Labels.User.Email"/></td>
                                    <td>
                                        <form:input type="text" path="email" cssClass="form-control" autocomplete="off"/><span><form:errors path="email" cssClass="error"/></span> 
                                    </td>
                                </tr>

                                <tr>
                                    <td><spring:message code="UI.Labels.User.Password"/></td>
                                    <td>
                                        <form:input type="password" path="password" cssClass="form-control" autocomplete="off"/><span></span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <button class="btn btn-primary" type="submit" name="submit">
                            <spring:message code="UI.Labels.User.Submit"/>
                        </button>

                    </form:form>
                </div>
            </div>

        </div>



        <%@include file="/WEB-INF/jspf/footer.jspf" %>
        
    </body>
</html>