<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <spring:message code="User.Edit" var="msgEdit"/>
        <title><c:out value="${initParam.applicationName} - ${msgEdit}"/></title>

        <%@include file="/WEB-INF/jspf/resources.jspf" %>
    </head>
    <body>
        <%@include file="/WEB-INF/jspf/header.jspf" %>
        <c:choose>
            <c:when test="${not empty insufficientRights}">
                <p><c:out value="${insufficientRights}"/></p>
            </c:when>


            <c:otherwise>

                <div class="col-md-5 col-md-offset-1">
                    <form:form id="edit-form" action="${pageContext.request.contextPath}/user/${user.id}/edit" method="post" commandName="user">
                        <table class="b-table table table-striped">
                            <tbody>
                                <tr>
                                    <td>
                                        <spring:message code="UI.Labels.User.FirstName"/>
                                    </td>
                                    <td><form:input type="text" path="firstName" cssClass="form-control" /></td>
                                </tr>
                                <tr>
                                    <td>
                                        <spring:message code="UI.Labels.User.LastName"/>
                                    </td>
                                    <td><form:input type="text" path="lastName" cssClass="form-control" /></td>
                                </tr>
                                <tr>
                                    <td>
                                        <spring:message code="UI.Labels.User.Role"/>
                                    </td>
                                    <td>
                                        <form:select path="userRole" cssClass="form-control">
                                            <form:options items="${roles}"/>
                                        </form:select>

                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        
                        <button class="btn btn-primary" type="submit" name="submit">
                            <spring:message code="UI.Labels.User.Submit"/>
                        </button>
                    </form:form>
                </div>

            </c:otherwise>
        </c:choose>                
        <%@include file="/WEB-INF/jspf/footer.jspf" %>

    </body>
</html>