package ru.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.test.jpa.User;

import java.util.List;

@Repository
public interface UserDAO extends JpaRepository<User, Long> {
    public User findByLogin(String login);

    @Query("SELECT u FROM User u WHERE u.firstName LIKE %?1% OR u.lastName LIKE %?1% or u.email LIKE %?1% or u.login LIKE %?1%")
    public List<User> search(String query);
}
