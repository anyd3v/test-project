package ru.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.test.jpa.SystemRole;

@Repository
public interface SystemRoleDAO extends JpaRepository<SystemRole, Long> {

    public SystemRole findByName(String login);
}
