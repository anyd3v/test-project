package ru.test.jpa;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "user_courses")
public class UserCourses implements Serializable{

    @Id
    @OneToOne
    @JoinColumn(name = "id")
    private User user;

    @Id
    @OneToOne
    @JoinColumn(name = "id")
    private Course course;


    @Enumerated(EnumType.STRING)
    private Status status;


    public enum Status {
        STUDYING,
        EXAMS,
        PASSED,
        FAILED
    }
}
